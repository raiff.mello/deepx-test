export interface Issue {
  id: string,
  city: string;
  createdAt: string;
  deletedAt: string;
  description: string;
  number: string;
  state: string;
  title: string;
  updatedAt: string;
  favorite: boolean;
};