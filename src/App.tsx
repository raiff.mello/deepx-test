import React from 'react';
import { Router, Switch, Route } from 'react-router-dom';
import './App.css';


import { createBrowserHistory } from 'history';
import { General } from './views/General';
import { Favorite } from './views/Favorite';
import { Typography } from '@material-ui/core';

const history = createBrowserHistory();

function App() {

  return (
    
    <div className='App'>
      <Typography
        variant='h5'
        color='textPrimary'
      >
        Deepx Issue List Challenge
      </Typography>
      
      <Router history={history}>
        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Route path='/favorites'>
            <Favorite />
          </Route>
          <Route path='/'>
            <General />
          </Route>
        </Switch>
    </Router>
    </div>
  );
}

export default App;
