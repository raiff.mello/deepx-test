import api from "../api";

export class IssueService {
    private static instance: IssueService;

    constructor() {
        if (!IssueService.instance) {
            IssueService.instance = this
        }
        return IssueService.instance
    }

    getIssues = (owner: string, repo: string) => {
        return api.get(`/${owner}/${repo}/issues`)
    }
}

const issueService = new IssueService()
export default issueService
