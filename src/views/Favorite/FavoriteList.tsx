import { Grid } from "@material-ui/core";

import type { FC } from "react";
import { useIssue } from "../../providers/issue";
import { IssueList } from "../../components/IssuesList";

const FavoritesList: FC = () => {
  const { favorites } = useIssue()
  return (
    <Grid container spacing={0}>
      <IssueList issueList={favorites}/>
    </Grid>
  )
}

export default FavoritesList 
