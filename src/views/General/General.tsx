import { Box, Button, Grid, TextField, makeStyles } from "@material-ui/core";
import { Link} from 'react-router-dom';
import classNames from 'classnames';

import { FC, useState } from "react";
import { useIssue } from "../../providers/issue";
import issueService from "../../services/issue.service";
import { IssueList } from "../../components/IssuesList";
import DoubleArrowIcon from '@material-ui/icons/DoubleArrow';
import { FilterSort } from "../../components/FilterSort";
import Alert from '@material-ui/lab/Alert';

const useStyles = makeStyles(() => ({
  
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalWrapper: {
    maxHeight: '100vh',
    maxWidth: 900,
    borderRadius: 6,
    backgroundColor: "#fff",
    border: '0.5px solid #c4c4c4',
    textAlign: "center",
    '&:focus': {
      outline: 'none'
    }
  },
  modalContent: {
    display: "flex",
    flexDirection: "column",
    textAlign: "center",
    marginTop: '5%',
    width: '32em',
    height: '14em'
  },
  title: {
    fontWeight: 'normal',
    fontSize: '22px',
    letterSpacing: '0.25px',
    color: '#5914DF',
    paddingBottom: '10px',
  },
  text: {
    color: '#282655',
    fontSize: 18,
    marginBottom: 30,
    fontFamily: 'Roboto',
    fontweight: '400px !important',
    lineheight: '20px !important',
    letter: '0.25px !important',
  },
  button: {
    margin: '0 10px 20px 10px',
  },
  buttonLink:{
    width: '150px',
    height: '34px',
  },
  buttonGetIssues:{
    width: '150px',
    backgroundColor: '#5850EC'
  },
  halfInput: {
    flex: 1,
    minWidth: 200,
    marginRight: 20,
    marginBottom: 20
  }

}));

const General: FC = () => {
  const { issues, setIssues } = useIssue()
  const [ owner, setOwner ] = useState('')
  const [ repo, setRepo ] = useState('')
  const [ error, setError ] = useState('')
  const [ hasError, setHasError ] = useState(false)

  const classes = useStyles();

  const getIssues = async () => {
    try {
      const { data } = await issueService.getIssues(owner, repo);
      setHasError(false)
      setIssues(data)
    } catch (err) {
      console.log(err)
      setIssues([])
      setError(err.message)
      setHasError(true)
    }
  }

  return (
    <Grid 
      container
      spacing={0}
      justify="center"
      alignItems="center">
      <Box display='flex' marginTop='1%' >
        <Box textAlign="center">
          <TextField 
            className={classes.halfInput} color="secondary" label="Owner" type="search" variant="outlined" 
            onChange={(e) => {setOwner(e.target.value)}}/>
        </Box>
        <Box textAlign="center">
          <TextField 
            className={classes.halfInput} color="secondary" label="Repositório" type="search" variant="outlined" 
            onChange={(e) => {setRepo(e.target.value)}}/>
        </Box>
      </Box>
      <Button
        variant="contained"
        color="secondary"
        className={classNames(classes.button, classes.buttonGetIssues)}
        onClick={getIssues}
        >
          Get Issues
      </Button> 
      <Link to="/favorites">
        <Button variant="contained"
          color="secondary"
          className={classNames(classes.button, classes.buttonGetIssues, classes.buttonLink)}>
            <DoubleArrowIcon />
            <p>Favorites</p>
        </Button>
        </Link>
        <Box>        
          {hasError &&
          <Alert severity="error">{error}</Alert>}
        </Box>
        <FilterSort />
      <IssueList issueList={issues}/>
    </Grid>
  )
}

export default General
