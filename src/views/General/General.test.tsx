import React from 'react';
import { render } from '@testing-library/react';
import General from './General';
import { BrowserRouter } from 'react-router-dom';

test('render messages', () => {
  const { getByText } = render(<BrowserRouter><General /></BrowserRouter>)
  expect(getByText('Get Issues')).toBeInTheDocument()
  expect(getByText('Favorites')).toBeInTheDocument()
})
