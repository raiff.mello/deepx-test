import React from 'react';
import { getByText, render, screen } from '@testing-library/react';
import App from './App';

test('renders learn react link', () => {
  const { getByText } = render(<App />);
  expect(getByText('Deepx Issue List Challenge')).toBeInTheDocument()
});
