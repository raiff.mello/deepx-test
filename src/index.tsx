import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { IssueProvider } from './providers/issue';

ReactDOM.render(
  <React.StrictMode>
    <IssueProvider>
        <App />
    </IssueProvider>
  </React.StrictMode>,
  document.getElementById('root')
);


