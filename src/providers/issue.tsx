import React, { useState } from "react";
import { Issue } from "../types/Issues";

interface AppContextInterface {
  issues?: Array<Issue>,
  setIssues: Function,
  favorites?: Array<Issue>,
  setFavorites: Function
}
export const IssueContext = React.createContext<AppContextInterface>({
  setIssues: () => {},
  setFavorites: () => {}
});

export const IssueProvider = (props: any ) => {
  const [issues, setIssues] = useState([])

  const [favorites, setFavorites] = useState([])

  return (
    <IssueContext.Provider value={{ issues, setIssues, favorites, setFavorites }}>
      {props.children}
    </IssueContext.Provider>
  )
}

export const useIssue = () => React.useContext(IssueContext)