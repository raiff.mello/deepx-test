import axios from 'axios';

export const BASE_URL = 'https://api.github.com/repos';
const api = axios.create({ baseURL: BASE_URL });

api.interceptors.response.use(
    (response) => response,
    (error) => Promise.reject((error.response && error.response.data) || 'Something went wrong')
);

export default api;