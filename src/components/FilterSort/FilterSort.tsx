import React, { FC } from 'react';
import { makeStyles } from '@material-ui/core/styles';

import { Box, FormControl, FormControlLabel, FormLabel, Radio, RadioGroup } from '@material-ui/core';
import { useWindowSize } from '../../hooks/useWindowsSize';
import { useIssue } from '../../providers/issue';
import { Issue } from '../../types/Issues';


const useStyles = makeStyles({
  root: {
    alignItems: 'flex-start',
    marginTop: '1%'
  },
  radio: {
    color: '#5850EC',
    '&.Mui-checked': {
      color: '#5850EC',
    }
  }
})

const FilterSort: FC = ( ) => {
  const classes = useStyles()
  const [width] = useWindowSize();
  const { setIssues } = useIssue();
  const { setFavorites } = useIssue();


  const handleChange = (e: any) => {
    if(e.target.value === 'id'){
      sortId()
    }else if(e.target.value === 'state'){
      sortState()
    }
  }
  const computedWidth = () => {
    const total = width - 120;
    return total > 600 ? 0 : total;
  };

  const sortId = () => {
    setIssues((prev: any) => {
      prev.sort((issue1: Issue, issue2: Issue) => parseInt(issue1.id) - parseInt(issue2.id));
      return [...prev]
    })
    setFavorites((prev: any) => {
      prev.sort((issue1: Issue, issue2: Issue) => parseInt(issue1.id) - parseInt(issue2.id));
      return [...prev]
    })
  }

  const sortState = () => {
    setIssues((prev: any) => {
      prev.sort((issue1: Issue, issue2: Issue) => (issue1.state > issue2.state ? -1 : 1));
      return [...prev]
    })
    setFavorites((prev: any) => {
      prev.sort((issue1: Issue, issue2: Issue) => (issue1.state > issue2.state ? -1 : 1));
      return [...prev]
    })
  }

  return (
    <Box className={classes.root} width={computedWidth() || '100%'}>

      <FormControl component='fieldset'>
        <FormLabel component='legend'>Sort By</FormLabel>
        <RadioGroup
          row
          aria-label='sortBy'
          name='row-radio-buttons-group'
          onChange={handleChange}>
          <FormControlLabel value='id' control={<Radio className={classes.radio} />} label='Id' />
          <FormControlLabel value='state' control={<Radio className={classes.radio} />} label='State' />
        </RadioGroup>
      </FormControl>
    </Box>
  );
}


export default FilterSort

