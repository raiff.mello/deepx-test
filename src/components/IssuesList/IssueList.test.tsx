import React from 'react';
import { render } from '@testing-library/react';
import IssueList from './IssueList';

test('render messages', () => {
  const { getByText } = render(<IssueList issueList={[]}/>)
  expect(getByText('ID')).toBeInTheDocument()
  expect(getByText('Number')).toBeInTheDocument()
  expect(getByText('Title')).toBeInTheDocument()
  expect(getByText('State')).toBeInTheDocument()
})
