import React, { FC } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Box, Typography } from '@material-ui/core';
import { useWindowSize } from '../../hooks/useWindowsSize';
import IssueState from './ListStateType';
import { useIssue } from '../../providers/issue';
import StarBorderOutlinedIcon from '@material-ui/icons/StarBorderOutlined';
import StarIcon from '@material-ui/icons/Star';

import { Issue } from '../../types/Issues';
import IconButton from '@material-ui/core/IconButton/IconButton';


const useStyles = makeStyles({
  root: {
    alignItems: "flex-start",
    marginTop: '2%'
  },
  table: {
    minWidth: 650,
  },
  formControl: {
    minWidth: 350,
    marginRight: 30,
    marginTop: 20,
  },
  formInput: {
    marginBottom: 15,
  },
  formInputArea: {
    height: "100%",
  },
  button: {
    marginTop: 20,
    width: 150
  },
  icon: {
    color: "rgba(0, 0, 0, 0.54)"
  },
  stateDescription: {
    fontSize: 12,
    letterSpacing: 0.1,
    color: "#7E7272"
  },
  id: {
    marginLeft: '3%',
    alignSelf: 'center',
    fontSize: 14,
    letterSpacing: 0.1,
    color: '#7E7272'
  },
  tableHeaderCell: {
    lineHeight: 1.57
  },
  star: {
    color: '#c4eb17',
    '&.Mui-checked': {
      color: '#5850EC',
    }
  }
});

interface ListComponentsProps {
  issueList:  Array<Issue> | undefined
}

const ListComponent: FC<ListComponentsProps> = ({ issueList = [] } ) => {
  const classes = useStyles();
  const [width] = useWindowSize();
  const { setIssues } = useIssue()
  const { setFavorites } = useIssue()

  const handleFavorite = (issue: Issue, index: number) => {
    issue.favorite = !issue.favorite

    if(issue.favorite) {
      setFavorites((prev: any) => {
        prev.push(issue)
        return [...prev]
      })
    } else{
      setFavorites((prev: any) => {
        prev.splice(index, 1);
        return [...prev]
      })
    }
    setIssues((prev: any)=>{
      prev[index] = issue
      return [...prev]
    })
  };
  
  const computedWidth = () => {
    const total = width - 120;
    return total > 600 ? 0 : total;
  };

  return (
    <Box className={classes.root} width={computedWidth() || "100%"}>
      <TableContainer>
        <Table className={classes.table} aria-label="caption table">
          <TableHead>
            <TableRow>
              <TableCell className={classes.tableHeaderCell}>
                ID
              </TableCell>
              <TableCell align="left" className={classes.tableHeaderCell}>
                Number
              </TableCell>
              <TableCell align="center" className={classes.tableHeaderCell}>
                Title
              </TableCell>
              <TableCell className={classes.tableHeaderCell}>
                State
              </TableCell>
              <TableCell align="right" className={classes.tableHeaderCell}>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {issueList && issueList.map((row, index) => (
              <TableRow key={row.id}>
                <TableCell align="left">
                  <Box display="flex">
                    <Typography
                      variant="caption"
                      className={classes.id}>
                        {row.id}
                    </Typography>
                  </Box>
                </TableCell>
                <TableCell align="left">{row.number}</TableCell>
                <TableCell align="center">{row.title}</TableCell>
                <TableCell align="left">
                      <Typography
                        variant="caption"
                        className={classes.stateDescription}>
                          State
                      </Typography>
                      <IssueState
                        stateType={row.state}
                      />
                </TableCell>
                <TableCell align="right">
                  <IconButton onClick={() => handleFavorite(row, index)}>
                  {row.favorite ? 
                    <StarIcon/> :
                    <StarBorderOutlinedIcon/>
                  }
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
  );
}


export default ListComponent

