import React, { FC } from "react";
import {
  Typography,
  makeStyles
} from "@material-ui/core";


interface IssueStateProps {
  stateType: string;
  fontSize?: number;
}

const useStyles = makeStyles(() => ({
  container: {
    maxWidth: "fit-content",

    textAlign: "center",
    borderRadius: 2
  },
  text: {
    fontWeight: 500,
    lineHeight: 1,
    textTransform: "uppercase",
    letterSpacing: 0.83
  }
}));

const IssueState: FC<IssueStateProps> = ({ stateType, fontSize }) => {
  const classes = useStyles();

  const makeContainer = (state: string, background: string, color: string) => {
    return (
      <div
        className={classes.container}
        style={{ background }}
      >
        <Typography
          variant="caption"
          className={classes.text}
          style={{ color, fontSize }}
        >
          {state}
        </Typography>
      </div>
    );
  }

  return (
    <>
      {stateType === 'open' &&
        makeContainer("open", "#FFF3E0", "#F57C00")
      }
      {stateType === 'closed' &&
        makeContainer("closed",  "rgba(235, 87, 87, 0.2)", "#E53C5B")
      }
      {stateType === 'all' &&
        makeContainer("all", "#E8F5E9", "#4CAF50")
      }
    </>
  );
}

export default IssueState;